# A quiz
Mobile life task. Quiz application based on Node.js/React

## System requirements

 - node.js
 - npm
 - git

## Installation

First you have to clone remote project repository
```sh
$ git clone https://bitbucket.org/vroskus/mobilelife-task.git mobilelife
```

Enter project directory
```sh
$ cd mobilelife
```

Install all dependencies
```sh
$ npm --prefix backend install && npm --prefix frontend install
```

Build frontend
```sh
$ npm run-script --prefix frontend build
```

## Configuration

Backend configuration file can be found here: `backend/config.json`
Frontend configuration file can be found here: `frontend/config.json`
A list of questions that will be used in the application can be defined here: `backend/questions.json`,

All files has predefined data, so the application can be run out of the box.

## Running the application

At first the backend server has to be started
```sh
$ npm run-script --prefix backend start
```
Most likely it will start on port 3001, if not please update frontend configuration.

Using any browser open `frontend/dist/index.html`

## Running tests

There are several test provided for both stacks

To test frontend you have to run
```sh
$ npm run-script --prefix frontend test
```

To test backend you have to run
```sh
$ npm run-script --prefix backend test
```

## Demo

A working demo can be found [here](http://regattas.eu:8122)
